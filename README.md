# External DAC MCP4725A3T with XIAO Demo

In this demo, the MCP4725A3T 12-bit DAC is used to generate analog voltage output. Now it has an [Texas LM4871 amplifier](https://www.ti.com/lit/ds/symlink/lm4871.pdf).

![External DAC Demo Board](Images/heroshot.jpg)

## Parts Used

- [Seeed XIAO RP2040](https://wiki.seeedstudio.com/XIAO-RP2040/)
- [Microchip MCP4725A3T](https://cdn-shop.adafruit.com/datasheets/mcp4725.pdf)
- [Texas Instruments LM4871](https://www.ti.com/lit/ds/symlink/lm4871.pdf)

Reference circuit from the MCP datasheet is used in the design of this demo board. 

## Arduino Dependencies

You will need to install the `Adafruit_MCP4725` library for your Arduino IDE. Also, follow the [XIAO RP2040 Arduino](https://wiki.seeedstudio.com/XIAO-RP2040-with-Arduino/) guide to set up the board definitions.

## Demoing

The idea is to program the board with the [Arduino/sinewave](Arduino/sinewave/sinewave.ino) code and monitor the DAC output using a logic analyzer. This has been tested at the Aalto Fablab using the [Salae Logic Pro 8](https://www.saleae.com/products/saleae-logic-pro-8).

You should be able to see a sinewave on an analog channel, similar to what can be seen in image below.

![Analyzer View](Images/analyzer.jpg)

## Troubleshooting

The initial motivation for this project was another board with this chip that was not working. A decision was made to investigate. It was discovered that the I2C address was wrong. Finally, the address was obtained using the I2C scanner code that can also be found in the [Arduino/i2c_scanner](Arduino/i2c_scanner/i2c_scanner.ino) project.

Another thing to keep in mind is that the last part of part number determines the initial address scope.

| Part Number | ADDR pin GND | ADDR pin VCC |
| --- | --- | --- |
| MCP4725 A0 | 0x60 | 0x61 |
| MCP4725 A1 | 0x62 | 0x63 |
| MCP4725 A2 | 0x64 | 0x65 |
| MCP4725 A3 | 0x66 | 0x67 |

## License 

This code is public domain but you buy me a beer if you use this and we meet someday (Beerware license).

Uses code by [Adafruit](https://learn.adafruit.com/mcp4725-12-bit-dac-tutorial).